===================
Chromite Bug Bounty
===================

Requirements
------------
- A computer running Ubuntu 20.04 with internet connection.

Tools Installation
------------------
For Manual Installation please refer `Installation Doc <https://incoresemiconductorspvtltd-my.sharepoint.com/:w:/g/personal/purushothaman_palani_incoresemi_com/EZsTEqAegK5JoNRs1sp0SVgB3vig_SjyqXU8IzTlXeHguA>`_

For automated Installation follow this.

- All the tools required for this activity will be installed automatically when you run the ``inst_toolchain.sh`` bash script found in this directory.
- The shell script can be run by using the ``source installation.sh`` command.
- This installation process will take a significant time and download ~5GB of files and compiles them. Feel free to go about your work while it installs.
- This script will download and install all the tools into a directory named ``chromite_bb`` at your ``\home\<user>\`` directory.
- Please make sure to check once in a whle to make sure the installation is progressing. You will be REQUIRED to accept licences, provide yes/no options in between.    
- Once you've finished running that shell script. Close your terminal and open a new terminal, cd to this same directory.
- Now run the ``python_env.sh`` file using the ``source python_env.sh`` command. This will create a conda environment with Python 3.7 and also buld chromite for you.
- Now you should have all the tools installed and set-up.
